package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MathServiceTest {
  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void testFibonacciOf5() {
    Integer body = this.restTemplate.getForObject("/api/math/fibonacci?val=5", Integer.class);
    assertThat(body).isNotNull().isEqualTo(5);
  }

  @Test
  public void testFibonacciOf8() {
    Integer body = this.restTemplate.getForObject("/api/math/fibonacci?val=8", Integer.class);
    assertThat(body).isNotNull().isEqualTo(21);
  }
  
  @Test
  public void testFibonacciOf12() {
    Integer body = this.restTemplate.getForObject("/api/math/fibonacci?val=12", Integer.class);
    assertThat(body).isNotNull().isEqualTo(144);
  }

  @Test
  public void testFibonacciOf0() {
    Integer body = this.restTemplate.getForObject("/api/math/fibonacci?val=0", Integer.class);
    assertThat(body).isNotNull().isEqualTo(0);
  }

}

package com.example.demo.controller;

import com.example.demo.exception.NumberNotValidException;
import com.example.demo.service.MathService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api/math")
public class MathController {
  @Autowired
  private MathService mathService;

  /**
   * Calcula el valor de fibonacci para el parametro <code>val</code> dado.
   * 
   * @param val El valor hasta el cual calcular fibonacci, por defecto es el
   *            número 5.
   * @return <code>ResponseEntity</code> con el valor entero de fibonacci
   *         correspondiente a <code>val</code>.
   */
  @RequestMapping(value = { "fibonacci" }, method = { RequestMethod.GET })
  public ResponseEntity<Integer> fibonacci(@RequestParam(required = false) Integer val) {
    try {
      return ResponseEntity.ok(this.mathService.fibonacci(val));
    } catch (NumberNotValidException e) {
      return ResponseEntity.badRequest().build();
    }
  }

}

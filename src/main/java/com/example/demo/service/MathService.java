package com.example.demo.service;

import com.example.demo.exception.NumberNotValidException;

import org.springframework.stereotype.Service;

@Service
public class MathService {

  /**
   * Calcula el valor de fibonacci para el parametro <code>val</code> dado.
   * 
   * @param val El valor hasta el cual calcular fibonacci, por defecto es el
   *            número 5.
   * @return El valor entero de fibonacci
   *         correspondiente a <code>val</code>.
   * @throws NumberNotValidException Cuando un número no válido es pasado como
   *                                 parámetro.
   */
  public Integer fibonacci(Integer val) throws NumberNotValidException {
    return null;
  }

}

package com.example.demo.exception;

public class NumberNotValidException extends Exception {
  public NumberNotValidException() {
    super();
  }

  public NumberNotValidException(String message) {
    super(message);
  }

  public NumberNotValidException(Integer val) {
    super("El número " + val + "Debe de ser un entero mayor o igual a 0.");
  }

}
